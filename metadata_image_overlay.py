# -*- coding: utf-8 -*-
"""
Created on Wed Jul 16 11:23:44 2014

@author: widemann
"""
#%% import libraries & define functions. 
from math import log, exp, tan, atan, pi, ceil
# http://stackoverflow.com/questions/14329691/covert-latitude-longitude-point-to-a-pixels-x-y-on-mercator-projection
# http://stackoverflow.com/questions/7490491/capture-embedded-google-map-image-with-python-without-using-a-browser
R = 6378137.0
EQUATOR_CIRCUMFERENCE = 2*pi*R
INITIAL_RESOLUTION = EQUATOR_CIRCUMFERENCE/256.0

def latlontopixels(lat, lon, zoom):
    "Takes latitude and longitude to pixels location px,py"
    mx = lon*(pi/180.0)*R
    latrad = lat*pi/180.0
    my = R*log(tan(pi/4.0 + latrad/2))
    res = INITIAL_RESOLUTION/(2**zoom)
    # important: pixel resolution
    px = (mx + pi*R)/res
    py = (my + pi*R)/res
    return px, py

def pixelstolatlon(px, py, zoom):
    "Takes pixels px,py to latitude and longitude"
    res = INITIAL_RESOLUTION/(2**zoom)
    mx = px*res - pi*R
    my = py*res - pi*R
    latrad = my/R
    lat = (180/pi)*(2*atan(exp(latrad)) - pi/2.0)
    lon = (mx/R)*(180/pi)
    return lat, lon

def createImagGrid(nx,ny,w,h,rot):
    "Forms an imaging grid" 

import pyproj 
import numpy as np
import sys

class imagingGridCorners:
    def __init__(self):
        self.topLeft = np.zeros((3,),float)
        self.topRight = np.zeros((3,),float)
        self.lowerLeft = np.zeros((3,),float)
        self.lowerRight = np.zeros((3,),float)
        
        
def find_corners(lat,lon,w,h,rot):
    """
    finds the corners of the rotated imaging grid in lats and lons
    
    Input: 
        lat, lon (degrees), 
        w=width in meters, 
        h=height, 
        rot is grid rotation angle from bearing north (clockwise rotation)
    
    output: The corners of the imaging grid.
            G.topLeft = [lat,lon]
            G.topRight = ...
            G.lowerLeft
            G.lowerRight
            
    Example: lat=37.46, lon=-122.17, w=2e3, h=4e3, rot=-34.76
             G = find_corners(lat,lon,w,h,rot)
    """
    G = imagingGridCorners()
    alt = 0.
    ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
    wgs84 = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    x,y,z = pyproj.transform(wgs84, ecef,lon,lat,alt, radians=False)
    up = np.array([x,y,z])
    up = up/np.linalg.norm(up)
    if up[2] <= 0:
        sys.stderr.write("This is only setup for the northern hemisperhe right now.")
        sys.exit(1)
    z_unit_vec = [0.,0.,1.]
    northing = z_unit_vec - up[2]*up
    tmp = np.linalg.norm(northing)
    if tmp == 0:
        sys.stderr.write("I'm not setup for north pole.")
        sys.exit(1)        
    northing = northing/tmp
    easting = np.cross(northing,up)    
    
    # rotations around the up axis with quaternions makes more sense to me here
    # but the results are the same. 
    cs = np.cos(np.deg2rad(rot))
    sn = np.sin(np.deg2rad(rot))
    col_dir = cs*easting + sn*northing # remember it's clockwise 
    row_dir = -sn*easting + cs*northing
    
    center = [x,y,z]
    topLeft = center -(w/2)*col_dir + (h/2)*row_dir
    topRight = center + (w/2)*col_dir + (h/2)*row_dir
    lowerLeft = center - (w/2)*col_dir - (h/2)*row_dir
    lowerRight = center + (w/2)*col_dir - (h/2)*row_dir
    
    tmp = np.hstack((topLeft,topRight,lowerLeft,lowerRight)).reshape(4,3)
    lonTmp, latTmp, hTmp = pyproj.transform(ecef, wgs84,tmp[:,0],tmp[:,1], tmp[:,2] , radians=False)
    G.topLeft = np.array([lonTmp[0],latTmp[0],hTmp[0]])
    G.topRight = np.array([lonTmp[1],latTmp[1],hTmp[1]])
    G.lowerLeft = np.array([lonTmp[2],latTmp[2],hTmp[2]])
    G.lowerRight = np.array([lonTmp[3],latTmp[3],hTmp[3]])
    return G
    
def getWebserviceCorners(G):
    """
    Gets the smallest bounding box for the imaging grid. This is needed
    because the google earth on other webservices only return non-rotated 
    imagery. 
    """
    webserviceCorners = imagingGridCorners()
    wLon = np.array([G.lowerLeft[0],G.lowerRight[0],G.topLeft[0],G.topRight[0]])
    wLat = np.array([G.lowerLeft[1],G.lowerRight[1],G.topLeft[1],G.topRight[1]])
    minLon = min(wLon); maxLon = max(wLon)
    minLat = min(wLat); maxLat = max(wLat)
    webserviceCorners.topLeft = np.array([minLon,maxLat])
    webserviceCorners.topRight = np.array([maxLon,maxLat])
    webserviceCorners.lowerLeft = np.array([minLon,minLat])
    webserviceCorners.lowerRight = np.array([maxLon,minLat])
    return webserviceCorners
    
#%% Example of how to use the above functions. 
from cStringIO import StringIO
import Image
import urllib
import matplotlib.pyplot as plt
# Determine how to pull an image centered at c=(lat,lon)
# with height, h, and width, w.

# the maximum number of pixels google will return is 640 x 640
maxsize = 640.
# the maximum zoom level for satellite imagery is 21.
zoom = 17.0 # zoom=18.0 is approx 60cm resolution, zoom=17 is about 1.2m
            # I think resolution doubles with each level. 
# What's the pixel resolution?
dx = INITIAL_RESOLUTION/(2**zoom)

# The grid points are
#xaxis = res*np.arange(-(mxPxX-1)/2, (mxPxX)/2) # careful, matlab needs the last point removed.
#w = (xaxis[-1]-xaxis[0])+res
#
#yaxis = res*np.arange(-(mxPxY-1)/2, (mxPxY)/2) 
#h = (yaxis[-1]-yaxis[0])+res
#
#X,Y = np.meshgrid(xaxis,yaxis)
#cs = np.cos(np.deg2rad(rot))
#sn = np.sin(np.deg2rad(rot))
#eastGrid = cs*X + sn*Y
#northGrid = -sn*X + cs*Y

#if width > 640 or height > 640:
#    sys.stderr("The maximum image size is 640x640\n Lower ")
#    
# Balt spi image. 
#latitudedegrees: 34.947363807682542
#longitudedegrees: 69.278069817951689
#widthmeters: 4000
#heightmeters: 2.000001024000000e+03
#rotationradians: -1.369033994771975
lat = 34.947363807682542
lon = 69.278069817951689
rot = (180/pi)*-1.369033994771975
w = 2e3
h = 4e3
G = find_corners(lat,lon,w,h,rot)
geC = getWebserviceCorners(G)
# convert all these coordinates to pixels
ulx, uly = latlontopixels(geC.topLeft[1], geC.topLeft[0], zoom)
lrx, lry = latlontopixels(geC.lowerRight[1],geC.lowerRight[0], zoom)

# calculate total pixel dimensions of final image
xpix = lrx - ulx
ypix = uly - lry

# calculate rows and columns
cols, rows = int(ceil(xpix/maxsize)), int(ceil(ypix/maxsize))
scale = 1
xx = np.arange(maxsize/2,cols*maxsize,maxsize)
m = maxsize - 120
yy = np.arange(m/2,rows*maxsize,m)

final = Image.new("RGB", (int(xx[-1]+maxsize/2), int(yy[-1]+m/2)))
for colInd in range(xx.size):
    for rowInd in range(yy.size):
        latn, lonn = pixelstolatlon(ulx + xx[colInd], uly - yy[rowInd], zoom)        
        position = ','.join((str(latn), str(lonn)))
        print "column %d; row %d; (lat,lon)=(%0.6f,%0.6f)" %(colInd,rowInd,latn,lonn)
        urlparams = urllib.urlencode({'center': position,
                                      'zoom': str(int(zoom)),
                                      'size': '%dx%d' % (maxsize,maxsize),
                                      'maptype': 'satellite',
                                      'sensor': 'false',
                                      'scale': scale})
        url = 'http://maps.google.com/maps/api/staticmap?' + urlparams
        f=urllib.urlopen(url)
        im=Image.open(StringIO(f.read()))
        final.paste(im, (int(xx[colInd]-maxsize/2), int(yy[rowInd]-m/2)))

final.show()


#final2 = Image.new("RGB", (int(yy[-1]+m/2),int(xx[-1]+maxsize/2)))
#for colInd in range(yy.size):
#    for rowInd in range(xx.size):
#        latn, lonn = pixelstolatlon(ulx + yy[colInd], uly - xx[rowInd], zoom)        
#        position = ','.join((str(latn), str(lonn)))
#        print "column %d; row %d; (lat,lon)=(%0.6f,%0.6f)" %(colInd,rowInd,latn,lonn)
#        urlparams = urllib.urlencode({'center': position,
#                                      'zoom': str(int(zoom)),
#                                      'size': '%dx%d' % (maxsize,maxsize),
#                                      'maptype': 'satellite',
#                                      'sensor': 'false',
#                                      'scale': scale})
#        url = 'http://maps.google.com/maps/api/staticmap?' + urlparams
#        f=urllib.urlopen(url)
#        im=Image.open(StringIO(f.read()))
#        final2.paste(im, (int(yy[rowInd]-m/2),int(xx[colInd]-maxsize/2)))
#
#final2.show()

class latLonImage(Image.Image):
    def __init__(self):
        super(latLonImage, self).__init__() 
        self.topLeftCorner = np.zeros((2,))
        self.lowerRightCorner = np.zeros((2,))
    
# lon,lat    
#geC.topLeft
#Out[519]: array([ 69.26295407,  34.96683198])
#pixelstolatlon(ulx+final.size[0], uly-final.size[1],zoom)
## lat,lon
#(34.925674576634826, 69.29728634577495)
#


scale = 1
#lat = 37.7994451  
#lon = -122.4439124
lat = 37.4568493
lon = -122.1773955
position = "%0.10f,%0.10f" %(lat,lon)
urlparams = urllib.urlencode({'center': position,
                              'zoom': "%d" %(zoom),
                              'size': '%dx%d' % (mxPxX, mxPxY),
                              'maptype': 'satellite',
                              'sensor': 'false',
                              'scale': scale})
url = 'http://maps.google.com/maps/api/staticmap?' + urlparams
f=urllib.urlopen(url)
im_sat = Image.open(StringIO(f.read()))

plt.imshow(im_sat)
plt.show()
#%% road map
urlparams = urllib.urlencode({'center': position,
                              'zoom': "%d" %(zoom),
                              'size': '%dx%d' % (mxPxX, mxPxY),
                              'maptype': 'roadmap',
                              'sensor': 'false',
                              'scale': scale})
url = 'http://maps.google.com/maps/api/staticmap?' + urlparams
f=urllib.urlopen(url)
im_road = Image.open(StringIO(f.read()))

plt.imshow(im_road)
plt.show()


#%% hybrid
#urlparams = urllib.urlencode({'center': position,
#                              'zoom': "%d" %(zoom),
#                              'size': '%dx%d' % (mxPxX, mxPxY),
#                              'maptype': 'hybrid',
#                              'sensor': 'false',
#                              'scale': scale})
#url = 'http://maps.google.com/maps/api/staticmap?' + urlparams
#f=urllib.urlopen(url)
#im_hybrid = Image.open(StringIO(f.read()))
#
#plt.imshow(im_hybrid)
#plt.show()

#%%
b = np.asarray(im_sat.convert())
c = np.asarray(im_road.convert())

tmp = np.ascontiguousarray(c).view(np.dtype((np.void, c.dtype.itemsize * c.shape[1])))
_, idx = np.unique(b, return_index=True)

