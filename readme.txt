This demonstrates how to pull google earth background imagery and the
region's associated metadata. An algorithm that uses both metadata and 
imagery for getting all of the roads in ECEF coordinates is investigated. 

David Widemann
July 12, 2014