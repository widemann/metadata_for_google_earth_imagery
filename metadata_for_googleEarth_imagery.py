# -*- coding: utf-8 -*-
"""
Created on Sat Jul 12 12:07:42 2014

This demonstrates how to pull google earth background imagery and the
associated metadata. An algorithms that use both metadata and imagery 
for getting all of the roads ECEF coordinates is investigated. 

@author: david widemann
"""
#%%
# This is just an example of how to pull an image. 
from cStringIO import StringIO
import Image
import urllib
import matplotlib.pyplot as plt

# palace of fine arts in SF
lat = 37.7994451  
lon = -122.4439124
sz_w = 640 # max size.
sz_h = 640
url = "http://maps.googleapis.com/maps/api/staticmap?center=%0.15f,%015f8&size=%dx%d&zoom=14&sensor=false" %(lat,lon,sz_w,sz_h)
buffer = StringIO(urllib.urlopen(url).read())
image = Image.open(buffer)

plt.imshow(image)
plt.show()

#%% 
# http://stackoverflow.com/questions/14329691/covert-latitude-longitude-point-to-a-pixels-x-y-on-mercator-projection
# http://stackoverflow.com/questions/7490491/capture-embedded-google-map-image-with-python-without-using-a-browser
# How to mosaick and image.
from math import log, exp, tan, atan, pi, ceil

EARTH_RADIUS = 6378137
EQUATOR_CIRCUMFERENCE = 2 * pi * EARTH_RADIUS
INITIAL_RESOLUTION = EQUATOR_CIRCUMFERENCE / 256.0
ORIGIN_SHIFT = EQUATOR_CIRCUMFERENCE / 2.0

def latlontopixels(lat, lon, zoom):
    mx = (lon * ORIGIN_SHIFT) / 180.0
    my = log(tan((90 + lat) * pi/360.0))/(pi/180.0)
    my = (my * ORIGIN_SHIFT) /180.0
    res = INITIAL_RESOLUTION / (2**zoom)
    # important: pixel resolution
    px = (mx + ORIGIN_SHIFT) / res
    py = (my + ORIGIN_SHIFT) / res
    return px, py

def pixelstolatlon(px, py, zoom):
    res = INITIAL_RESOLUTION / (2**zoom)
    mx = px * res - ORIGIN_SHIFT
    my = py * res - ORIGIN_SHIFT
    lat = (my / ORIGIN_SHIFT) * 180.0
    lat = 180 / pi * (2*atan(exp(lat*pi/180.0)) - pi/2.0)
    lon = (mx / ORIGIN_SHIFT) * 180.0
    return lat, lon

############################################
# a neighbourhood in Lajeado, Brazil:

upperleft =  '-29.44,-52.0'  
lowerright = '-29.45,-51.98'
zoom = 18   # be careful not to get too many images!

############################################

ullat, ullon = map(float, upperleft.split(','))
lrlat, lrlon = map(float, lowerright.split(','))

# Set some important parameters
scale = 1
maxsize = 640

#%%
# convert all these coordinates to pixels
ulx, uly = latlontopixels(ullat, ullon, zoom)
lrx, lry = latlontopixels(lrlat, lrlon, zoom)

# calculate total pixel dimensions of final image
dx, dy = lrx - ulx, uly - lry

# calculate rows and columns
cols, rows = int(ceil(dx/maxsize)), int(ceil(dy/maxsize))

# calculate pixel dimensions of each small image
bottom = 120
largura = int(ceil(dx/cols))
altura = int(ceil(dy/rows))
alturaplus = altura + bottom

final = Image.new("RGB", (int(dx), int(dy)))
for x in range(cols):
    for y in range(rows):
        dxn = largura * (0.5 + x)
        dyn = altura * (0.5 + y)
        latn, lonn = pixelstolatlon(ulx + dxn, uly - dyn - bottom/2, zoom)
        position = ','.join((str(latn), str(lonn)))
        print x, y, position
        urlparams = urllib.urlencode({'center': position,
                                      'zoom': str(zoom),
                                      'size': '%dx%d' % (largura, alturaplus),
                                      'maptype': 'satellite',
                                      'sensor': 'false',
                                      'scale': scale})
        url = 'http://maps.google.com/maps/api/staticmap?' + urlparams
        f=urllib.urlopen(url)
        im=Image.open(StringIO(f.read()))
        print "column %d; row %d" %(x,y)
        final.paste(im, (int(x*largura), int(y*altura)))
final.show()

#%%
zoom_ratios = {
                '20' : 1128.497220,
                '19' : 2256.994440,
                '18' : 4513.988880,
                '17' : 9027.977761,
                '16' : 18055.955520,
                '15' : 36111.911040,
                '14' : 72223.822090,
                '13' : 144447.644200,
                '12' : 288895.288400,
                '11' : 577790.576700,
                '10' : 1155581.153000,
                '9'  : 2311162.307000,
                '8'  : 4622324.614000,
                '7'  : 9244649.227000,
                '6'  : 18489298.450000,
                '5'  : 36978596.910000,
                '4'  : 73957193.820000,
                '3'  : 147914387.600000,
                '2'  : 295828775.300000,
                '1'  : 591657550.500000
            }



